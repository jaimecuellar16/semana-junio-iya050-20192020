const { React, ReactDOM } = window;


class CustomGame extends React.Component{
    constructor(props){
        super(props);
        this.updateGame = this.updateGame.bind(this);

        this.jugarCustom = this.jugarCustom.bind(this);
        this.activateCell = this.activateCell.bind(this);
        this.nextGeneration = this.nextGeneration.bind(this);
        this.next = this.next.bind(this);
        this.state = {
            data: 
            [[0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0]],
             jugando:false,
        };
    }
    nextGeneration(){
        fetch('http://localhost:8080/life/next',{
            method:"POST",
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res =>res.json())
        .catch(error => console.log(error))
        .then(response => {
            console.log(this.state.data);
            this.setState({data:response});
            this.updateGame();
        });
        
    }
    updateGame(){
        console.log(this.state.data);
        let generation = this.state.data;
        for (let i=0; i<generation.length;i++){
            for (let j=0; j<generation[i].length;j++){
                if(generation[i][j]==1){
                    document.getElementById(`${i},${j}`).className="selected-cell";
                }
                else{
                    if(document.getElementById(`${i},${j}`).classList.contains("selected-cell")){
                        document.getElementById(`${i},${j}`).classList.remove("selected-cell");
                    }
                }
            }
        }
    }
    next(){
        this.nextGeneration();
    }
    jugarCustom(){
        this.setState({jugando:!this.state.jugando});
        fetch('http://localhost:8080/life/',{
            method:"POST",
            body: JSON.stringify(this.state.data),
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res => res.json())
        .catch(error => console.error("error", error))
        .then(response => {
            this.setState({data:response});
            this.updateGame();
        });
    }
    
    activateCell(event){
        console.log(event.target.id);
        const parts = event.target.id.split(",");
        let x = parts[0];
        let y = parts[1];
        let arr = this.state.data;
        arr[x][y] = 1;
        console.log(arr);
        document.getElementById(event.target.id).className = "selected-cell";
        this.setState({data:arr});
    }
    render(){
        return(
                <div className="gamePad">
                       <table className="table" cellSpacing="0" border="1">
                           <tbody>
                            <tr>
                                <td id="0,0" onClick={this.activateCell}></td>
                                <td id="0,1" onClick={this.activateCell}></td>
                                <td id="0,2" onClick={this.activateCell}></td>
                                <td id="0,3" onClick={this.activateCell}></td>
                                <td id="0,4" onClick={this.activateCell}></td>
                            </tr>
                            <tr>
                                <td id="1,0" onClick={this.activateCell}></td>
                                <td id="1,1" onClick={this.activateCell}></td>
                                <td id="1,2" onClick={this.activateCell}></td>
                                <td id="1,3" onClick={this.activateCell}></td>
                                <td id="1,4" onClick={this.activateCell}></td>
                            </tr>
                                <tr>
                                <td id="2,0"onClick={this.activateCell}></td>
                                <td id="2,1" onClick={this.activateCell}></td>
                                <td id="2,2" onClick={this.activateCell}></td>
                                <td id="2,3" onClick={this.activateCell}></td>
                                <td id="2,4" onClick={this.activateCell}></td>
                            </tr>
                                <tr>
                                <td id="3,0" onClick={this.activateCell}></td>
                                <td id="3,1" onClick={this.activateCell}></td>
                                <td id="3,2" onClick={this.activateCell}></td>
                                <td id="3,3" onClick={this.activateCell}></td>
                                <td id="3,4" onClick={this.activateCell}></td>
                            </tr>
                                <tr>
                                <td id="4,0" onClick={this.activateCell}></td>
                                <td id="4,1" onClick={this.activateCell}></td>
                                <td id="4,2" onClick={this.activateCell}></td>
                                <td id="4,3" onClick={this.activateCell}></td>
                                <td id="4,4" onClick={this.activateCell}></td>
                            </tr>
                           </tbody>
                       </table>
                       {
                           !this.state.jugando ?
                            <div>
                                <button className="button" onClick={this.jugarCustom}>A Jugar!</button>
                            </div>
                         :null
                       }
                       {
                           this.state.jugando ?
                           <button className="button" onClick={this.next}> Siguiente </button>
                           :null
                       }
            </div>
        )
    }
}