const { React, ReactDOM, CustomGame, DefaultGame } = window;

class Game extends React.Component{
    constructor(props){
        super(props);
        this.iniciar = this.iniciar.bind(this);
        this.iniciarCustom = this.iniciarCustom.bind(this);
        this.activateCell = this.activateCell.bind(this);
        this.jugarCustom = this.jugarCustom.bind(this);
        this.jugar = this.jugar.bind(this);
        this.updateGame = this.updateGame.bind(this);
        this.nextGeneration = this.nextGeneration.bind(this);
        this.state = {
            iniciar : false,
            iniciarCustom: false,
            data: 
            [[0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0]],
             jugando:false
        };
    }

    iniciar(){
        console.log("iniciando el juego");
        this.setState({iniciar: !this.state.iniciar});
    }
    iniciarCustom(){
        console.log("iniciando custom");
        this.setState({iniciarCustom: !this.state.iniciarCustom});
    }
    jugarCustom(){
        fetch('http://localhost:8080/life/',{
            method:"POST",
            body: JSON.stringify(this.state.data),
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res => res.json())
        .catch(error => console.error("error", error))
        .then(response => console.log(response));
    }
    jugar(){
        fetch('http://localhost:8080/life/')
        .then(res => res.json())
        .catch(error => console.log(error))
        .then(response => {
            console.log(response);
            this.setState({data:response});
            console.log(this.state.data);
            this.setState({jugando:true,iniciar:!this.state.iniciar});
            this.updateGame();
        });
    }

    updateGame(){
        const generation = this.state.data;
        for (let i=0; i<generation.length;i++){
            for (let j=0; j<generation[i].length;j++){
                if(generation[i][j]==1){
                    document.getElementById(`${[i]},${[j]}`).className="selected-cell";
                }
            }
        }

        this.nextGeneration();
    }
    activateCell(event){
        console.log(event.target.id);
        const parts = event.target.id.split(",");
        let x = parts[0];
        let y = parts[1];
        let arr = this.state.data;
        arr[x][y] = 1;
        console.log(arr);
        document.getElementById(event.target.id).className = "selected-cell";
        this.setState({data:arr});
    }

    nextGeneration(){
        fetch('http://localhost:8080/life/next',{
            method:"POST",
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res =>res.json())
        .catch(error => console.log(error))
        .then(response => console.log(response));
    }
    render(){
        return(
            <div>
                <div className="game-options">
                    {
                        !this.state.iniciar && !this.state.iniciarCustom ? 
                        <button className="button" onClick={this.iniciar}>
                            Iniciar juego.
                        </button>
                        :null
                    }
                    {
                        !this.state.iniciarCustom && !this.state.iniciar? 
                        <button className="button" onClick={this.iniciarCustom}>
                            Iniciar con un estado inicial
                        </button>
                        : null
                    }
                </div>
               {
                   this.state.iniciar ? 
                   <div className="custom-game">
                       <DefaultGame/>
                   </div>
                   : null
               }
               {
                   this.state.iniciarCustom ? 
                   <div className="custom-game">
                       <CustomGame/>
                    </div>
                   : null
               }
            </div>
        );
    }
}