const { React, ReactDOM } = window;


export class DefaultGame extends React.Component{
    constructor(props){
        super(props);
        this.jugar = this.jugar.bind(this);
        this.updateGame = this.updateGame.bind(this);
        this.nextGeneration = this.nextGeneration.bind(this);
        this.next = this.next.bind(this);
        this.state = {
            data: 
            [[0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0],
             [0,0,0,0,0]],
        }
    }
    componentDidMount(){
        this.jugar();
    }

    next(){
        this.nextGeneration();
    }
    jugar(){
        fetch('http://localhost:8080/life/')
        .then(res => res.json())
        .catch(error => console.log(error))
        .then(response => {
            this.setState({data:response});
            this.updateGame();
        });
    }

    updatePad(){
        fetch('http://localhost:8080/life/',{
            method:"POST",
            body: JSON.stringify(this.state.data),
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res => res.json())
        .catch(error=> console.log(error))
        .then(response =>{
            console.log(response);
            this.setState({data:response});
            this.jugar();
        });
    }

    updateGame(){
        console.log(this.state.data);
        let generation = this.state.data;
        for (let i=0; i<generation.length;i++){
            for (let j=0; j<generation[i].length;j++){
                if(generation[i][j]==1){
                    document.getElementById(`${i},${j}`).className="selected-cell";
                }
                else{
                    if(document.getElementById(`${i},${j}`).classList.contains("selected-cell")){
                        document.getElementById(`${i},${j}`).classList.remove("selected-cell");
                    }
                }
            }
        }
    }
    nextGeneration(){
        fetch('http://localhost:8080/life/next',{
            method:"POST",
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res =>res.json())
        .catch(error => console.log(error))
        .then(response => {
            console.log(this.state.data);
            this.setState({data:response});
            this.updateGame();
        });
        
    }
    render(){
        return(
                <div className="gamePad">
                       <table className="table" cellSpacing="0" border="1">
                           <tbody>
                            <tr>
                                <td id="0,0" ></td>
                                <td id="0,1" ></td>
                                <td id="0,2" ></td>
                                <td id="0,3" ></td>
                                <td id="0,4" ></td>
                            </tr>
                            <tr>
                                <td id="1,0" ></td>
                                <td id="1,1" ></td>
                                <td id="1,2" ></td>
                                <td id="1,3" ></td>
                                <td id="1,4" ></td>
                            </tr>
                                <tr>
                                <td id="2,0" ></td>
                                <td id="2,1" ></td>
                                <td id="2,2" ></td>
                                <td id="2,3" ></td>
                                <td id="2,4" ></td>
                            </tr>
                                <tr>
                                <td id="3,0" ></td>
                                <td id="3,1" ></td>
                                <td id="3,2" ></td>
                                <td id="3,3" ></td>
                                <td id="3,4" ></td>
                            </tr>
                                <tr>
                                <td id="4,0" ></td>
                                <td id="4,1" ></td>
                                <td id="4,2" ></td>
                                <td id="4,3" ></td>
                                <td id="4,4" ></td>
                            </tr>
                           </tbody>
                       </table>
                    <button className="button" onClick={this.next}>Siguiente</button>
            </div>
        )
    }
}