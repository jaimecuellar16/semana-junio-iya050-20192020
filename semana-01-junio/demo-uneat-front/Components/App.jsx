const { React, ReactDOM, Home, Game } = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/home">Home</Link>
              </li>
              <li>
                <Link to="/jugar">Jugar</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route exact path="/" component={() => <Redirect to="/home" />} />
            <Route path="/home" component={Home}></Route>
            <Route path="/jugar" component={Game}></Route>
          </Switch>
        </div>
      </Router>
    );
  }
}
